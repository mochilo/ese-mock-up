import Vue from 'vue';
import App from './component/App.vue';

new Vue({ render: createElement => createElement(App) }).$mount('#app');
